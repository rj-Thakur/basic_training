                     _________COMMANDS_________

1. ls -> used to list out all the file and folder for the particular 
   directory (-r recursive, -a for all, -s list out with size of the file)

2. mkdir -> to create new folder

3. cat -> this command use to perform various operation over the file 
          1. cat fileName -> to show file 
          2. cat > fileName -> create and write in a file
          3. cat file1 > file2 -> copy one file content into another file 
          4. cat "f1","f2","f3" > mergeFile -> to merge multiple file into one
          5. cat >> file -> to write an existing file

4. dpkg -> this command use for install "Debian" package

5. kill -> to terminate ongoing process

6. lsof -> list out all open file for the loged-in user

7. netstat -> to display active connection that is currently running on computer

8. vim vs nano -> vim and nano both are editor to edit the file

9. sudo vs normal user
    1. sudo -> sudo means sub user do so sudo means user has extra power that can change system file and many more
    2. normal user ->  normal user can only perform simple task and not able to change into system file.



                   ___________GIT___________

1. local repository -> it means a repository that you create or initialize into your local computer

2. remote repository -> it means a repo that your created into your git or github account

3. config  -> to see the basic configuration fo git into you system

4. clone -> using colone command you can easily bring git or github repo content into your local repo

5. fetch -> this command is only tell that there are some changes in remote repo but not bring that changes into local repo

6. pull ->  this command tell that there are some changes and bring those changes into local

7. push -> this command use to push you local repo code into remote repo

8. add -> it used to stage all changes those are present into your local

9. commit -> it is use to add comment into you staged file

10. reset -> it is use to undo locals changes for tha particular repo

11. rm -> to use remove files from the repo or to make staged files to untracked file

12. amend -> using amend command you can edit latest commit in the local repo

13. diff -> to see the diff. between two branches or files

14. log -> it loges all commits for the particular branch

15. stash -> this command save your uncommitted changes for later use in any other branch

16. revert -> this command is also used to undo commit form the repo but it is diff. form reset because it generate new commit it repo that start form tha revert keyword

                     ___________WEB TERMS__________

1. Authentication -> In web authentication is a process which verify the user according to the credentials provided by the user

2. Authorization -> Authorization is a process which determine the role of a specific user

3. HTTP -> HTTP is a hyper text transfer protocol use to bring html file content form the server

4. HTTPS -> It is upgraded version of HTTP where s is stand for secure it provide encryption while send information over the internet

5. Client server architecture -> Client server arch. means

            _________________________________________________________
           |                                             _________   |                 
           |   _______________                          |\_______/|  |
           |  |               |       request           ||       ||  |
           |  |     CLIENT    | ----------------------> ||       ||  |
           |  |               |                         || server||  |
           |  |_______________| <---------------------- ||       ||  |
           |        _/_\_               response        ||       ||  |                                                             
           |                                            ||_______||  |
           |_________________________________________________________|
           
 6. HTTP Verbs ->  GET ->  getting date form the web server
                   POST -> sending date to the web server
                   PUT -> update date on the web server
                   DELETE -> delete date on the server


                   _________GIT ADVANCE ________

1. Branch -> Branch is a pointer which points your update work. 
                when you create a branch pointer points to the new branch where you can able to make change without affecting previous branch                   

2. checkout -> checkout is a command which use to create a new branc 
                       git checkout -b <branch name>
               and to use switching between one to another brach
                       git checkout <branch name>

3. cherry-pick -> this command use to pick particular command from the branch to add into specific branch.
                   we can pick branch using hash code of the branch

4. Merge -> merge command is use to merge tow branch 
            branch1  -->  master
            branch2  -->  feature

            if we want to merge feature branch into master branch we use git merge feature

5. Rebase -> it is most similar command as merge but rebase generate a linear graph.

6. [squash, reword, edit] -> these are some option which provided by the rebase command to perform various option
                 
                 1. squash -> it use to meal one commit into another commit.
                 2. reword -> it use to edit commit but not the content of the commit. 
                 3. edit   -> it us use to edit commit as well as content of the commit.                                          